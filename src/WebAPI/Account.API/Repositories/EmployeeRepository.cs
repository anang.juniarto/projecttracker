using Account.API.Commons;
using Account.API.Contexts;
using Account.Domain.Entities;

namespace Account.API.Repositories;

public class EmployeeRepository : BaseRepository<EmployeeEntity>
{
    private readonly AccountContext _accountContext;

    public EmployeeRepository(AccountContext accountContext) : base(accountContext)
    {
        this._accountContext = accountContext;
    }
}
