using System.Text;
using System.Text.Json;
using Account.API.Contexts;
using RabbitMQ.Client;

namespace Account.API.Repositories;

/*
    Description	: Class Description.
    Author		: Anang.
    Created By	: Anang.				Created On	: Tuesday, 15 August 2023.
    Updated By	: Anang.				Updated On	: Tuesday, 15 August 2023.
    Version		: 1.0.0.
*/
public class UnitOfWork
{
    private readonly AccountContext _accountContext;

    private readonly ConnectionFactory _rabbitMQFactory;

    private UserRepository _userRepository;
    private EmployeeRepository _employeeRepository;

    public UnitOfWork(AccountContext accountContext)
    {
        this._accountContext = accountContext;

        _rabbitMQFactory = new ConnectionFactory
        {
            Uri = new Uri("amqp://guest:guest@127.0.0.1:5672")
        };
    }

    public void PublishRabbitMQ(string routingKey, string data)
    {
        using var connection = _rabbitMQFactory.CreateConnection();
        using var channel = connection.CreateModel();
        channel.QueueDeclare(routingKey,
            durable: true,
            exclusive: false,
            autoDelete: false,
            arguments: null);

        var body = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(data));
        channel.BasicPublish(
            exchange: "",
            routingKey: routingKey,
            mandatory: true,
            basicProperties: null,
            body: body);
    }

    public UserRepository UserRepository => _userRepository ??= new UserRepository(_accountContext);
    public EmployeeRepository EmployeeRepository => _employeeRepository ??= new EmployeeRepository(_accountContext);
}
