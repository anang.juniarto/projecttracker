using Account.API.Commons;
using Account.API.Contexts;
using Account.API.Helpers;
using Account.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Account.API.Repositories;

public class UserRepository : BaseRepository<UserEntity>
{
    private readonly AccountContext _accountContext;

    public UserRepository(AccountContext accountContext) : base(accountContext)
    {
        this._accountContext = accountContext;
    }

    public async Task<bool> UsernameExists(string username, long excludeID = 0)
    {
        var predicate = PredicateHelper.True<UserEntity>();
        predicate = predicate.And(user => user.Username.Trim().ToLower() == username.Trim().ToLower());
        if (excludeID > 0)
        {
            predicate = predicate.And(user => user.ID != excludeID);
        }
        return await _accountContext.User.AsNoTracking().AnyAsync(predicate);
    }

    public async Task<bool> EmailExists(string email, long excludeID= 0)
    {
        var predicate = PredicateHelper.True<UserEntity>();
        predicate = predicate.And(user => user.Email.Trim().ToLower() == email.Trim().ToLower());
        if (excludeID > 0)
        {
            predicate = predicate.And(user => user.ID != excludeID);
        }

        return await _accountContext.User.AsNoTracking().AnyAsync(predicate);
    }

    public async Task<UserEntity> GetUserWithEmployeeByID(long id)
    {
        return await _accountContext.User.AsNoTracking().AsSplitQuery()
            .Include(user => user.Employee)
            .Where(user => user.ID == id)
            .SingleOrDefaultAsync();
    }
}
