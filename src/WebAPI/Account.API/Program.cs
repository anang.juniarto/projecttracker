using System.Reflection;
using Account.API.Commons;
using Account.API.Contexts;
using Account.API.Repositories;
using MediatR;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

var config = new ConfigurationBuilder();
    config.AddCommandLine(args);
    config.AddJsonFile($"appsettings.json", optional: false, reloadOnChange: true);
    config.AddJsonFile($"appsettings.{builder.Environment}.json", optional: true, reloadOnChange: true);
    builder.Configuration.AddConfiguration(config.Build());

// Add services to the container.
string stringDatabaseConnection = builder.Configuration["ConnectionStrings:Account"];

ServerVersion serverVersion = ServerVersion.AutoDetect(stringDatabaseConnection);

builder.Services.AddDbContext<AccountContext>(
    options => options.UseMySql(stringDatabaseConnection, serverVersion,
    b => b.MigrationsAssembly(typeof(AccountContext).Assembly.FullName))
);

// builder.Services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddMediatR(cfg=>cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));

builder.Services.AddScoped(typeof(BaseRepository<>));
builder.Services.AddScoped<UnitOfWork>();
builder.Services.AddScoped<UserRepository>();
builder.Services.AddScoped<EmployeeRepository>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Host.UseContentRoot(Directory.GetCurrentDirectory());

builder.WebHost.UseKestrel(x =>
        x.ListenAnyIP(1000)
    );

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Apply migrations.
using (var scope = app.Services.CreateScope())
{
    var context = scope.ServiceProvider.GetRequiredService<AccountContext>();
    context.Database.Migrate();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
