using Account.API.Commons;
using MediatR;

namespace Account.API.Features.User.Requests.Queries;

/*
    Description	: Query for get all record.
    Author		: Anang.
    Created By	: Anang.				Created On	: Tuesday, 15 August 2023.
    Updated By	: Anang.				Updated On	: Tuesday, 15 August 2023.
    Version		: 1.0.0.
*/
public class GetAllUserQuery : IRequest<ResponseDto>
{


    /*
        Description	: Constructor.
        Author		: Anang.
        Created By	: Anang.				Created On	: Tuesday, 15 August 2023.
        Updated By	: Anang.				Updated On	: Tuesday, 15 August 2023.
        Version		: 1.0.0.
    */
    public GetAllUserQuery()
    {
        
    }
}
