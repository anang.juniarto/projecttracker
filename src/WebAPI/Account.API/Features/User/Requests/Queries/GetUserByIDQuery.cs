using Account.API.Commons;
using MediatR;

namespace Account.API.Features.User.Requests.Queries;

public class GetUserByIDQuery : BaseIDDto, IRequest<ResponseDto>
{
    
}
