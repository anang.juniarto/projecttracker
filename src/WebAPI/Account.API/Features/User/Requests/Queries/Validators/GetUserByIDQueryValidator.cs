using Account.API.Repositories;
using FluentValidation;

namespace Account.API.Features.User.Requests.Queries.Validators;

public class GetUserByIDQueryValidator : AbstractValidator<GetUserByIDQuery>
{
    public GetUserByIDQueryValidator(UnitOfWork unitOfWork)
    {
        RuleFor(request => request.ID).Cascade(CascadeMode.Stop)
            .MustAsync(async (id, cancellationToken) =>
            {
                return await unitOfWork.UserRepository.Exists(id);
            }).WithMessage("User not found.");
    }
}