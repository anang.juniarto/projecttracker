using Account.API.Enums;
using Account.API.Repositories;
using FluentValidation;

namespace Account.API.Features.User.Requests.Commands.Validators;

public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
{
    public CreateUserCommandValidator(UnitOfWork unitOfWork)
    {
        RuleFor(request => request.Username).Cascade(CascadeMode.Stop)
            .NotNull().NotEmpty().MaximumLength(100)
            .MustAsync(async (username, cancellationToken) =>
            {
                return await unitOfWork.UserRepository.UsernameExists(username);
            }).WithMessage("Username already exists.");

        RuleFor(request => request.Email).Cascade(CascadeMode.Stop)
            .NotNull().NotEmpty().MaximumLength(250)
            .MustAsync(async (email, cancellationToken) =>
            {
                return await unitOfWork.UserRepository.EmailExists(email);
            }).WithMessage("Email already exists.");
        
        RuleFor(request => request.EmployeeRole).Cascade(CascadeMode.Stop)
            .Must(employeeRole =>
            {
                return Enum.IsDefined(typeof(EmployeeRole), employeeRole);
            }).WithMessage("Employee role not defined.");
    }
}