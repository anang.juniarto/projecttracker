using Account.API.Repositories;
using FluentValidation;

namespace Account.API.Features.User.Requests.Commands.Validators;

public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
{
    public DeleteUserCommandValidator(UnitOfWork unitOfWork)
    {
        Include(new Features.User.Requests.Queries.Validators.GetUserByIDQueryValidator(unitOfWork));
    }
}