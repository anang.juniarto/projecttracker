using Account.API.Enums;
using Account.API.Repositories;
using FluentValidation;

namespace Account.API.Features.User.Requests.Commands.Validators;

public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
{
    public UpdateUserCommandValidator(UnitOfWork unitOfWork)
    {
        Include(new Features.User.Requests.Queries.Validators.GetUserByIDQueryValidator(unitOfWork));

        RuleFor(request => request.Username).Cascade(CascadeMode.Stop)
            .NotNull().NotEmpty().MaximumLength(100)
            .MustAsync(async (request, username, cancellationToken) =>
            {
                return await unitOfWork.UserRepository.UsernameExists(username, request.ID);
            }).WithMessage("Username already exists.");

        RuleFor(request => request.Email).Cascade(CascadeMode.Stop)
            .NotNull().NotEmpty().MaximumLength(250)
            .MustAsync(async (request, email, cancellationToken) =>
            {
                return await unitOfWork.UserRepository.EmailExists(email, request.ID);
            }).WithMessage("Email already exists.");
        
        RuleFor(request => request.EmployeeRole).Cascade(CascadeMode.Stop)
            .Must(employeeRole =>
            {
                return Enum.IsDefined(typeof(EmployeeRole), employeeRole);
            }).WithMessage("Employee role not defined.");
    }
}