using Account.API.Commons;
using MediatR;

namespace Account.API.Features.User.Requests.Commands;

public class CreateUserCommand : IRequest<ResponseDto>
{
    public string Username { get; set; }
    public string Email { get; set; }
    public int EmployeeRole { get; set; }
}
