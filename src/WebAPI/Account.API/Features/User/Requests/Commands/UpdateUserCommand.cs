using Account.API.Commons;
using Account.API.Features.User.Requests.Queries;
using MediatR;

namespace Account.API.Features.User.Requests.Commands;

public class UpdateUserCommand : GetUserByIDQuery, IRequest<ResponseDto>
{
    public string Username { get; set; }
    public string Email { get; set; }
    public int EmployeeRole { get; set; }
}
