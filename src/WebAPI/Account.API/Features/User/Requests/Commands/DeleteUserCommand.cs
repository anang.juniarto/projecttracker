using Account.API.Commons;
using Account.API.Features.User.Requests.Queries;
using MediatR;

namespace Account.API.Features.User.Requests.Commands;

public class DeleteUserCommand : GetUserByIDQuery, IRequest<ResponseDto>
{
    
}
