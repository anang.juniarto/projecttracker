using Account.API.Commons;
using Account.API.Enums;
using Account.API.Features.User.Requests.Queries;
using Account.API.Repositories;
using MediatR;

namespace Account.API.Features.User.Handlers.Queries;

public class GetAllUserQueryHandler : IRequestHandler<GetAllUserQuery, ResponseDto>
{
    private readonly UnitOfWork _unitOfWork;

    public GetAllUserQueryHandler(UnitOfWork unitOfWork)
    {
        this._unitOfWork = unitOfWork;
    }

    public async Task<ResponseDto> Handle(GetAllUserQuery request, CancellationToken cancellationToken)
    {
        var response = new ResponseDto();
        try
        {
            var result = await _unitOfWork.UserRepository.GetAll();
            response.Data = result;
        }
        catch (Exception ex)
        {
            response.StatusCode = (int)HTTPStatus.InternalServerError;
            response.Message = ex.Message;
        }

        return response;
    }
}
