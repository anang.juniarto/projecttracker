using Account.API.Commons;
using Account.API.Enums;
using Account.API.Features.User.Requests.Queries;
using Account.API.Features.User.Requests.Queries.Validators;
using Account.API.Repositories;
using MediatR;

namespace Account.API.Features.User.Handlers.Queries;

public class GetUserByIDQueryHandler : IRequestHandler<GetUserByIDQuery, ResponseDto>
{
    private readonly UnitOfWork _unitOfWork;

    public GetUserByIDQueryHandler(UnitOfWork unitOfWork)
    {
        this._unitOfWork = unitOfWork;
    }

    public async Task<ResponseDto> Handle(GetUserByIDQuery request, CancellationToken cancellationToken)
    {
        var response = new ResponseDto();

        try
        {
            var validators = new GetUserByIDQueryValidator(_unitOfWork);
            var validationResult = await validators.ValidateAsync(request);
            if (validationResult.IsValid)
            {
                var result = await _unitOfWork.UserRepository.GetByID(request.ID);
                response.Data = result;
            }
            else
            {
                response.StatusCode = (int)HTTPStatus.BadRequest;
                response.Message = validationResult.Errors.FirstOrDefault().ErrorMessage;
            }
        }
        catch (Exception ex)
        {
            response.StatusCode = (int)HTTPStatus.InternalServerError;
            response.Message = ex.Message;
        }

        return response;
    }
}
