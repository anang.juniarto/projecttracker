using Account.API.Commons;
using Account.API.Enums;
using Account.API.Features.User.Requests.Commands;
using Account.API.Features.User.Requests.Commands.Validators;
using Account.API.Repositories;
using MediatR;

namespace Account.API.Features.User.Handlers.Commands;

public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, ResponseDto>
{
    private readonly UnitOfWork _unitOfWork;

    public UpdateUserCommandHandler(UnitOfWork unitOfWork)
    {
        this._unitOfWork = unitOfWork;
    }

    public async Task<ResponseDto> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
    {
        var response = new ResponseDto();
        try
        {
            var validators = new UpdateUserCommandValidator(_unitOfWork);
            var validationResult = await validators.ValidateAsync(request);
            if (validationResult.IsValid)
            {
                var userSearch = await _unitOfWork.UserRepository.GetUserWithEmployeeByID(request.ID);
                userSearch.Username = request.Username;
                userSearch.Email = request.Email;
                if (userSearch.Employee != null)
                {
                    userSearch.Employee.EmployeeRole = (EmployeeRole)request.EmployeeRole;
                    await _unitOfWork.EmployeeRepository.Update(userSearch.Employee);
                    userSearch.Employee = null;
                }
                await _unitOfWork.UserRepository.Update(userSearch);
                response.Message = "User updated.";
            }
            else
            {
                response.StatusCode = (int)HTTPStatus.BadRequest;
                response.Message = validationResult.Errors.FirstOrDefault().ErrorMessage;
            }
        }
        catch (Exception ex)
        {
            response.StatusCode = (int)HTTPStatus.InternalServerError;
            response.Message = ex.Message;
        }

        return response;
    }
}
