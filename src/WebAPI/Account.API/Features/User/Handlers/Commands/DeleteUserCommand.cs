using Account.API.Commons;
using Account.API.Enums;
using Account.API.Features.User.Requests.Commands;
using Account.API.Features.User.Requests.Commands.Validators;
using Account.API.Repositories;
using MediatR;

namespace Account.API.Features.User.Handlers.Commands;

public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, ResponseDto>
{
    private readonly UnitOfWork _unitOfWork;

    public DeleteUserCommandHandler(UnitOfWork unitOfWork)
    {
        this._unitOfWork = unitOfWork;
    }

    public async Task<ResponseDto> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
    {
        var response = new ResponseDto();
        try
        {
            var validators = new DeleteUserCommandValidator(_unitOfWork);
            var validationResult = await validators.ValidateAsync(request);
            if (validationResult.IsValid)
            {
                var userDelete = await _unitOfWork.UserRepository.GetUserWithEmployeeByID(request.ID);
                if (userDelete.Employee != null)
                {
                    await _unitOfWork.EmployeeRepository.Delete(userDelete.Employee);
                    userDelete.Employee = null;
                }
                await _unitOfWork.UserRepository.Delete(userDelete);
                response.StatusCode = (int)HTTPStatus.Ok;
                response.Message = "User deleted.";
            }
            else
            {
                response.StatusCode = (int)HTTPStatus.BadRequest;
                response.Message = validationResult.Errors.FirstOrDefault().ErrorMessage;
            }
        }
        catch (Exception ex)
        {
            response.StatusCode = (int)HTTPStatus.InternalServerError;
            response.Message = ex.Message;
        }

        return response;
    }
}
