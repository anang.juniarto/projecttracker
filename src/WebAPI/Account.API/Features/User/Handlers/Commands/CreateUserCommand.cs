using System.Text.Json;
using Account.API.Commons;
using Account.API.Enums;
using Account.API.Features.User.Requests.Commands;
using Account.API.Features.User.Requests.Commands.Validators;
using Account.API.Repositories;
using Account.Domain.Entities;
using MediatR;

namespace Account.API.Features.User.Handlers.Commands;

public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, ResponseDto>
{
    private readonly UnitOfWork _unitOfWork;

    public CreateUserCommandHandler(UnitOfWork unitOfWork)
    {
        this._unitOfWork = unitOfWork;
    }

    public async Task<ResponseDto> Handle(CreateUserCommand request, CancellationToken cancellationToken)
    {
        var response = new ResponseDto();
        try
        {
            var validators = new CreateUserCommandValidator(_unitOfWork);
            var validationResult = await validators.ValidateAsync(request);
            if (validationResult.IsValid)
            {
                var userCreate = await _unitOfWork.UserRepository.Create(new UserEntity()
                {
                    Username = request.Username.Trim().ToLower(),
                    Email = request.Email.Trim().ToLower(),
                    Employee = new EmployeeEntity()
                    {
                        EmployeeRole = (EmployeeRole)request.EmployeeRole
                    }
                });
                if (userCreate != null)
                {
                    _unitOfWork.PublishRabbitMQ("create-user", JsonSerializer.Serialize(userCreate));
                }
                
                response.StatusCode = (int)HTTPStatus.Created;
                response.Message = "User created.";
            }
            else
            {
                response.StatusCode = (int)HTTPStatus.BadRequest;
                response.Message = validationResult.Errors.FirstOrDefault().ErrorMessage;
            }
        }
        catch (Exception ex)
        {
            response.StatusCode = (int)HTTPStatus.InternalServerError;
            response.Message = ex.Message;
        }

        return response;
    }
}
