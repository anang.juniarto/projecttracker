using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Account.API.Commons;
using Microsoft.EntityFrameworkCore;

namespace Account.Domain.Entities;

/*
    Description	: Entity of User.
    Author		: Anang.
    Created By	: Anang.				Created On	: Friday, 11 August 2023.
    Updated By	: Anang.				Updated On	: Friday, 11 August 2023.
    Version		: 1.0.0.
*/
[Table("User")]
[Index(nameof(Username), IsUnique = true)]
[Index(nameof(Email), IsUnique = true)]
public class UserEntity : AuditableEntity
{
    /*
        Description	: Declaration required service or variable.
        Author		: Anang.
        Created By	: Anang.				Created On	: Friday, 11 August 2023.
        Updated By	: Anang.				Updated On	: Friday, 11 August 2023.
        Version		: 1.0.0.
    */
    [Key]
    public long ID { get; set; } = 0;

    [StringLength(100)]
    public string Username { get; set; } = null;

    [StringLength(250)]
    public string Email { get; set; } = null;

    public EmployeeEntity Employee { get; set; }
}
