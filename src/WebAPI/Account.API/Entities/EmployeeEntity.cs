using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Account.API.Commons;
using Account.API.Enums;
using Microsoft.EntityFrameworkCore;

namespace Account.Domain.Entities;

/*
    Description	: Entity of Employee.
    Author		: Anang.
    Created By	: Anang.				Created On	: Friday, 11 August 2023.
    Updated By	: Anang.				Updated On	: Friday, 11 August 2023.
    Version		: 1.0.0.
*/
[Table("Employee")]
public class EmployeeEntity : AuditableEntity
{
    /*
        Description	: Declaration required service or variable.
        Author		: Anang.
        Created By	: Anang.				Created On	: Friday, 11 August 2023.
        Updated By	: Anang.				Updated On	: Friday, 11 August 2023.
        Version		: 1.0.0.
    */
    [Key]
    public long ID { get; set; } = 0;

    public long UserID { get; set; } = 0;
    [ForeignKey(nameof(UserID))]
    public UserEntity User { get; set; }

    public EmployeeRole EmployeeRole { get; set; } = EmployeeRole.Admin;
}
