namespace Account.API.Enums;

public enum HTTPStatus
{
    Ok = 200,
    Created = 201,
    BadRequest = 400,
    InternalServerError = 500
}