namespace Account.API.Enums;

public enum EmployeeRole
{
    Admin = 1,
    Developer = 2,
    QC = 3
}