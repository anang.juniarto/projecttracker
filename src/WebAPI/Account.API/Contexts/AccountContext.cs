using Account.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Account.API.Contexts;

/*
    Description	: Context of Account.
    Author		: Anang.
    Created By	: Anang.				Created On	: Tuesday, 15 August 2023.
    Updated By	: Anang.				Updated On	: Tuesday, 15 August 2023.
    Version		: 1.0.0.
*/
public class AccountContext : DbContext
{
    public DbSet<UserEntity> User { get; set; }

    public AccountContext(DbContextOptions<AccountContext> options) : base(options)
    {
        
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        
    }
}
