using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Account.API.Controllers;

[ApiController]
[Route("[controller]")]
public class EmployeeController : ControllerBase
{
    private readonly ILogger<EmployeeController> _logger;
    private readonly IMediator _mediator;

    public EmployeeController(ILogger<EmployeeController> logger, IMediator mediator)
    {
        _logger = logger;
        _mediator = mediator;
    }

    [HttpGet]
    [Route("All")]
    public async Task<IActionResult> All()
    {
        var response = await _mediator.Send(new Features.User.Requests.Queries.GetAllUserQuery());
        return StatusCode(response.StatusCode, response);
    }

    [HttpGet]
    public async Task<IActionResult> Get([FromQuery] long id)
    {
        var response = await _mediator.Send(new Features.User.Requests.Queries.GetUserByIDQuery()
        {
            ID = id
        });
        return StatusCode(response.StatusCode, response);
    }
}
