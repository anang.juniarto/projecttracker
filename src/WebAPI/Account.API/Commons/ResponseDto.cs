using System.Text.Json.Serialization;
using Account.API.Enums;

namespace Account.API.Commons;

/*
    Description	: DTO for response features.
    Author		: Anang.
    Created By	: Anang.				Created On	: Tuesday, 15 August 2023.
    Updated By	: Anang.				Updated On	: Tuesday, 15 August 2023.
    Version		: 1.0.0.
*/
public class ResponseDto
{
    [JsonIgnore]
    public int StatusCode { get; set; } = (int)HTTPStatus.Ok;
    public string Message { get; set; } = string.Empty;
    public object Data { get; set; } = null;
}
