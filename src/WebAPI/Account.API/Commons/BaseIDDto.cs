namespace Account.API.Commons;

public class BaseIDDto
{
    public long ID { get; set; } = 0;
}