using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Account.API.Commons;

/*
    Description	: Base of Auditable.
    Author		: Anang.
    Created By	: Anang.				Created On	: Friday, 11 August 2023.
    Updated By	: Anang.				Updated On	: Friday, 11 August 2023.
    Version		: 1.0.0.
*/
public class AuditableEntity
{
    /*
        Description	: Declaration required service or variable.
        Author		: Anang.
        Created By	: Anang.				Created On	: Friday, 11 August 2023.
        Updated By	: Anang.				Updated On	: Friday, 11 August 2023.
        Version		: 1.0.0.
    */
    public DateTime CreatedOn { get; set; } = DateTime.Now;

    [StringLength(100)]
    public string CreatedBy { get; set; } = string.Empty;

    public DateTime? UpdatedOn { get; set; } = null;

    [StringLength(100)]
    public string UpdatedBy { get; set; } = null;


    /*
        Description	: Constructor.
        Author		: Anang.
        Created By	: Anang.				Created On	: Friday, 11 August 2023.
        Updated By	: Anang.				Updated On	: Friday, 11 August 2023.
        Version		: 1.0.0.
    */
    public AuditableEntity()
    {
        
    }
}
