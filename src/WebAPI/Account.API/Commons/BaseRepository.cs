using Account.API.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Account.API.Commons;

public class BaseRepository<T> where T : class
{
    private readonly AccountContext _context;

    public BaseRepository(AccountContext context)
    {
        this._context = context;
    }

    public async Task<bool> Exists(long id)
    {
        return await GetByID(id) != null;
    }

    public async Task<T> GetByID(long ID)
    {
        return await _context.Set<T>().FindAsync(ID);
    }

    public async Task<List<T>> GetAll()
    {
        return await _context.Set<T>().ToListAsync();
    }

    public async Task<T> Create(T entity)
    {
        await _context.Set<T>().AddAsync(entity);
        var intResult = await _context.SaveChangesAsync();
        if (intResult > 0)
        {
            return entity;
        }
        else
        {
            return null;
        }
    }

    public async Task<int> Update(T entity)
    {
        int id = 0;
        int.TryParse(entity.GetType().GetProperty("ID").GetValue(entity).ToString(), out id);
        if (id > 0)
        {
            this.DetachEntityTracking(id);
        }
        _context.Entry(entity).State = EntityState.Modified;
        return await _context.SaveChangesAsync();
    }

    public async Task<int> Delete(T entity)
    {
        int id = 0;
        int.TryParse(entity.GetType().GetProperty("ID").GetValue(entity).ToString(), out id);
        if (id > 0)
        {
            this.DetachEntityTracking(id);
        }
        _context.Set<T>().Remove(entity);
        return await _context.SaveChangesAsync();
    }
    public void DetachEntityTracking(long ID)
    {
        foreach (var entity in _context.ChangeTracker.Entries<T>().Where(x => x.Property("ID").CurrentValue.ToString() == ID.ToString()))
        {
            _context.Entry(entity.Entity).State = EntityState.Detached;
            _context.SaveChanges();
        }
    }
}
